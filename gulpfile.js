/** CONFIG SECTION **/

var indexInjectFiles = [
    /*JQuery*/
    'node_modules/jquery/dist/jquery.min.js',

    /* MomentJS */
    'node_modules/moment/min/moment.min.js',

    /* AngularJS */
    'node_modules/angular/angular.min.js',
    'node_modules/angular-ui-router/release/angular-ui-router.min.js',
    'node_modules/angular-ui-validate/src/validate.js',
    'node_modules/angular-cookies/angular-cookies.min.js',
    'node_modules/angular-animate/angular-animate.js',

    /* ArriveJS */
    'node_modules/arrive/minified/arrive.min.js',

    /* Ng-focus-if */
    'node_modules/ng-focus-if/focusIf.min.js',

    /* App*/
    'app/app.js',
    'app/**/*.js',
    'app/**/*.html'
];
var buildFiles = indexInjectFiles.concat([
    "content/css/main.css",
    "content/images/favicon.ico",
    "index.html"
]);

/** REQUIRE SECTION **/
var gulp = require("gulp");
var inject = require('gulp-inject');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');

/** TASK SECTION **/
gulp.task('compile-sass', function () {
    return gulp.src('./content/scss/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./content/css'));
});

gulp.task("inject-index", function () {
    var target = gulp.src('./index.html');
    var sources = gulp.src(indexInjectFiles, {read: false});
    return target.pipe(inject(sources)).pipe(gulp.dest('./'));
});

gulp.task("build", ["compile-sass", "inject-index"], function () {
    var options = {
        base: "."
    };
    return gulp.src(
        buildFiles
        , options).pipe(gulp.dest("build"));

});