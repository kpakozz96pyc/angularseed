// Remove element from array
Array.prototype.remove = function (value) {
    var index = this.indexOf(value);
    if (index >= 0)
        this.splice(index, 1);
};

// Map array to another array using selector expression
Array.prototype.select = function(expr){
    return this.map(expr);
};

// Map array to array of arrays using selector expression
Array.prototype.selectMany = function (expr) {
    // hack because reduce can't work with empty arrays
    if (this.length == 0)
        return this.map(expr);

    return this.map(expr).reduce(function (a, b) {
        return a.concat(b);
    });
};

// Clone object to another
Object.clone = function(old, deepClone) {
    return $.extend(deepClone, {}, old);
};

// Check whether is object defined and not null
Object.exist = function(obj) {
    return angular.isDefined(obj) && obj != null;
};

// Check whether is object has some own properties
Object.isEmpty = function isEmpty(map) {
    for(var key in map) {
        if (map.hasOwnProperty(key)) {
            return false;
        }
    }
    return true;
};

function isObject(item) {
    return (item && typeof item === 'object' && !Array.isArray(item) && item !== null);
};

// Merge two objects
Object.merge = function(target, source) {
    if (isObject(target) && isObject(source)) {
        for (const key in source) {
            if (isObject(source[key]) && !Object.isEmpty(source[key])) {
                if (!target[key]) Object.assign(target, { [key]: {} });
                Object.merge(target[key], source[key]);
            } else {
                Object.assign(target, { [key]: source[key] });
            }
        }
    }
    return target;
};





