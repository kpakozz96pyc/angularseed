(function () {

    angular.module('seed.core', ['ngCookies'])
        .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider) {

            $stateProvider
                .state('home', {
                    url: '/',
                    views: {
                        'main': {
                            templateUrl: 'app/core/templates/home.html',
                            controller: 'HomeController',
                            controllerAs: 'controller'
                        }
                    }
                })
                .state('404', {
                    url: '/404',
                    views:{main:{
                        templateUrl: 'app/core/templates/notfound.html'
                    }},
                    public: true
                });
        }]);
}());