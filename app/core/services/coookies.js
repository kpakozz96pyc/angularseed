"use strict";

(function () {

    angular.module("seed.core")
        .factory("SeedCookies", SeedCookies);

    SeedCookies.$inject = ['$cookies'];

    function SeedCookies($cookies) {
        return {
            Get : get,
            Set : set,
            Remove : remove
        }

        function set(name, value, expires) {
            if (angular.isNumber(expires)) {
                var lifetime = expires * 1000;
                expires = new Date();
                expires.setTime(expires.getTime() + lifetime);
            }

            if (angular.isDate(expires)) {
                expires = expires.toUTCString();
            }

            $cookies.put(name, value, {'expires': expires});
            return value;
        }

        function get(name) {
            return $cookies.get(name);
        }

        function remove(name) {
            var expires = -1;
            return set(name, null, expires);
        }
    }

})();