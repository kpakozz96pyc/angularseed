"use strict";

/** ********  ATTENTION!  *************************************************************************************
 * THIS FILE IS USED BY Tools->CFG_Transform->Client->Gulpfile.js FOR SETTING THE REAL ENVIRONMENT HOST
 * IF you change MAIN_HOST_URL constant or move this file to another folder then change gulpfile.js too please.
 * **/

(function () {
    angular.module("seed.core")
        .constant("MAIN_HOST_URL", "http://localhost:20822")
        .service('SeedUrls', SeedUrls);
    
    SeedUrls.$inject =
        [
            'MAIN_HOST_URL'
        ];
    function SeedUrls(MAIN_HOST_URL) {
        var self = this;

        return self;
    }
})();