var express = require("express");
var server = express();

server.use("/node_modules", express.static("./node_modules"));
server.use("/content/images/favicon.ico", express.static("./content/images/favicon.ico"));
server.use("/content/css", express.static("./content/css"));
server.use("/app", express.static("./app"));
server.get("/*", redirectToIndex);
server.listen(8003);

function redirectToIndex(request, result) {

    var options = {
        root: __dirname
    };

    result.sendFile("index.html", options);
}